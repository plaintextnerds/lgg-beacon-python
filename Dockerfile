FROM python:3.10-rc-slim as base

WORKDIR /app
RUN pip install --upgrade pip poetry==1.1.7
COPY ./src/poetry.lock /app/poetry.lock
COPY ./src/pyproject.toml /app/pyproject.toml
RUN apt-get update

FROM base as builder
RUN apt-get install -y build-essential libffi-dev
RUN poetry install

COPY ./src /app
ENV PYTHONPATH=/app
RUN poetry build

FROM base as runtime
COPY --from=builder /app /app
RUN poetry install --no-dev
CMD [ "poetry", "run", "beacon" ]
