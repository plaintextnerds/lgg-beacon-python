SHELL:=$(PREFIX)/bin/sh

DISTRO?=ubuntu-any
GIT_TAG?=v0.0.0-local

rebuild: clean build

build: out/beacon_${DISTRO}_${GIT_TAG}.deb

clean:
	rm -rf .tmp/${DISTRO} out

out/beacon_${DISTRO}_${GIT_TAG}.deb: .tmp/${DISTRO}
	mkdir -p $(@D)
	dpkg-deb -b $< $@

.tmp/${DISTRO}: packages/${DISTRO} src/dist/*.whl
	mkdir -p $(@D)
	cp -r $< $@
	mkdir -p $@/etc/latency-machine/
	cp $(word 2,$^) $@/etc/latency-machine/
	sed -i 's/Version:.*/Version: '$(patsubst v%,%,${GIT_TAG})'/' $@/DEBIAN/control

.PHONY: build rebuild clean
.INTERMEDIATE: .tmp