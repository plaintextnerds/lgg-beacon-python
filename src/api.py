"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""
import json
from typing import Optional, List, Tuple
from urllib.parse import urljoin

import requests
from nacl.encoding import Base64Encoder  # type: ignore
from nacl.signing import VerifyKey  # type: ignore
from twisted.internet import threads, ssl  # type: ignore
from twisted.internet.defer import succeed  # type: ignore
from twisted.web.client import Agent, BrowserLikePolicyForHTTPS  # type: ignore
from twisted.web.http_headers import Headers  # type: ignore
from twisted.web.iweb import IBodyProducer, IPolicyForHTTPS  # type: ignore
from zope.interface import implementer  # type: ignore


def register_beacon(
    api: str,
    location: str,
    provider: str,
    version: str,
    ipv4: str,
    ipv6: Optional[str],
    token: str,
) -> Tuple[str, Optional[VerifyKey]]:
    jdata = {
        "location": location,
        "provider": provider,
        "version": version,
        "ipv4": ipv4,
    }
    headers = {
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
    }
    if ipv6:
        jdata["ipv6"] = ipv6
    response = requests.post(
        urljoin(api, "register"),
        headers=headers,
        json=jdata,
    )
    response.raise_for_status()
    data = response.json()
    verify_key = None
    if data.get("pub"):
        verify_key = VerifyKey(Base64Encoder.decode(data.get("pub")))
    return data["beacon"], verify_key


def submit_measurements(reactor, api: str, uuid: str, samples: List[dict], token: str):
    policy = WhitelistContextFactory([b'beacon.latency.gg', b'beacon.latency.dev'])
    agent = Agent(reactor, contextFactory=policy)
    response = threads.blockingCallFromThread(
        reactor,
        agent.request,
        b"POST",
        urljoin(api, f"record/{uuid}").encode(),
        Headers(
            {
                b"Authorization": [f"Bearer {token}".encode()],
                b"Content-Type": [b"application/json"],
            }
        ),
        BytesProducer(json.dumps(samples).encode()),
    )
    if response.code not in [200, 201, 202]:
        print(f"push error: {response.code}")


@implementer(IBodyProducer)
class BytesProducer:
    def __init__(self, body):
        self.body = body
        self.length = len(body)

    def startProducing(self, consumer):
        consumer.write(self.body)
        return succeed(None)

    def pauseProducing(self):
        pass

    def stopProducing(self):
        pass


@implementer(IPolicyForHTTPS)
class WhitelistContextFactory:
    def __init__(self, good_domains=None):
        if not good_domains:
            self.good_domains = []
        else:
            self.good_domains = good_domains

        # by default, handle requests like a browser would
        self.default_policy = BrowserLikePolicyForHTTPS()

    def creatorForNetloc(self, hostname, port):
        # check if the hostname is in the the whitelist, otherwise return the default policy
        if hostname in self.good_domains:
            return ssl.CertificateOptions(verify=False)
        return self.default_policy.creatorForNetloc(hostname, port)