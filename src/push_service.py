"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""
from datetime import datetime
from statistics import mean, stdev

from twisted.internet import threads  # type: ignore

from database import Database, MILLISECONDS
from api import submit_measurements


class Push:
    def __init__(self, api: str, uuid: str, token: str, database: Database):
        self.api = api
        self.uuid = uuid
        self.token = token
        self.database = database
        self.previous_timestamp = datetime.now().timestamp() - 1

    def in_thread(self, reactor):
        threads.deferToThread(self.push, reactor)

    def push(self, reactor):
        current_timestamp = datetime.now().timestamp() - 1
        with self.database.read_txn() as txn:
            for i in range(int(current_timestamp - self.previous_timestamp)):
                samples = self.database.get_samples_by_timestamp(
                    txn, (self.previous_timestamp + i) * MILLISECONDS
                )
                records = [
                    {
                        "type": "udp-data",
                        "version": sample.version,
                        "ip": sample.ip.addr.decode(),
                        "pub": sample.pub.decode(),
                        "rtt": int(
                            mean(
                                [
                                    sample.timestamp_c1 - sample.timestamp_c0,
                                    sample.timestamp_c2 - sample.timestamp_c1,
                                    sample.timestamp_s1 - sample.timestamp_s0,
                                    sample.timestamp_s2 - sample.timestamp_s1,
                                ]
                            )
                        ),
                        "stddev": int(
                            stdev(
                                [
                                    sample.timestamp_c1 - sample.timestamp_c0,
                                    sample.timestamp_c2 - sample.timestamp_c1,
                                    sample.timestamp_s1 - sample.timestamp_s0,
                                    sample.timestamp_s2 - sample.timestamp_s1,
                                ]
                            )
                        ),
                        "raw": sample.as_dict,
                    }
                    for sample in samples
                ]
                submit_measurements(reactor, self.api, self.uuid, records, self.token)
        self.previous_timestamp = current_timestamp
